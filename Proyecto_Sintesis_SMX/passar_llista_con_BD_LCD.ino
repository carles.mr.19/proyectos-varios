//Carles Miranda 23-02-2017

#include <Ethernet2.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include "Keypad.h"
#include <LiquidCrystal_I2C.h>

  LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 
  const byte ROWS = 4;
  const byte COLS = 4;
  char keyInsert[7];
  char Keys[ROWS][COLS]= {
    {'1','2','3','A'} ,
    {'4','5','6','B'},
    {'7','8','9','C'},
    {'*','0','#','D'}
  };
  byte colPins[4] = {5,4,3,2};
  byte rowPins[4] = {9,8,7,6};
  int i = 0;
  int j = 0;
  Keypad keypad = Keypad( makeKeymap(Keys), rowPins, colPins, ROWS, COLS);
  byte mac_addr[] = {};     //Mac Addres Arduino Ethernet
  IPAddress server_addr();  // IP MySQL
  char user[] = "";         // Usuario MySQL
  char password[] = "";     // Contraseña MySQL
  long idusuari = 0;
  const char QUERY_POP[] = "SELECT * FROM grup1.codigos as AK Where AK.codigo='%s';";
  char query[55];
  char server[] = ""; //URL Servidor
  EthernetClient client;
  EthernetClient client2;
  MySQL_Connection conn((Client *)&client);
  
  void setup() {
      Serial.begin(115200);
      tone(A3, 1000, 200);
      while (!Serial);
      Ethernet.begin(mac_addr);
      delay(2000);
               
      if (conn.connect(server_addr, 3306, user, password)) {
          Serial.println(F("INS CAMPCLAR - www.inscampclar.cat - Passar Llista v1.1 2016-2017"));
          Serial.println();
          Serial.println(F("Connectant amb el servidor..."));  
        
          lcd.begin(16, 2);
          lcd.setCursor(0,0);
          lcd.print(F("INS CAMPCLAR"));
          lcd.setCursor(0,1);
          lcd.print(F("CONNECTANT"));
      } else {
          Serial.println(F("Ha fallado la conexión"));
      }

  }

  void loop() {
      char key = keypad.getKey();
      
      if (i==0){
        Serial.println();
        lcd.clear();
        Serial.println(F("Introduce el codigo de 6 digitos y haz clien en la tecla *"));
        lcd.setCursor(0,0);
        lcd.print(F("CODIGO ALUMNE"));
        lcd.setCursor(0,1);
        lcd.print(F("DE 6 DIGITOS"));
        i++;
      }

      if (j==1){
        lcd.setCursor(0,0);
        lcd.print(F("RECUERDA 6 DIGITOS"));
        lcd.setCursor(0,1);
        lcd.print(F("*                "));
      }

      if (key != NO_KEY && j<6){
        lcd.backlight();
        lcd.setCursor(0,1);
        Serial.print(F("*"));
        lcd.setCursor(j,1);
        lcd.print(F("*"));
        keyInsert[j]=key;
        tone(A3, 2100, 75);

        j++;
      }

      if(key == '*' or j==6) {
        Serial.println();
        Serial.println(F("Verificando el codigo..."));
        lcd.setCursor(0,0);
        lcd.print(F("UN MOMENTO      "));
        lcd.setCursor(0,1);
        lcd.print(F("VERIFICANDO CODIGO"));
     
        if(isAuthorized(keyInsert)){
          Serial.println(F("Correcto"));
          tone(A3, 1915, 75);
          delay(201);
          tone(A3, 2100, 75);
          delay(201);
          tone(A3, 1915, 75);
          delay(201);
        } else {

          Serial.println(F("El codigo no existeix, contacta con el tutor"));
          lcd.setCursor(0,0);
          lcd.print(F("ERROR EN EL CODIGO"));
          lcd.setCursor(0,1);
          lcd.print(F("CONTACTA TUTOR"));
          tone(A3, 1000, 300);
          delay(501);
        }

        i=0;
        j=0;
        delay(2000);

      } 

      if(key =='#'){
        lcd.noBacklight();     
      }
  }

  boolean isAuthorized(char code[]){
      MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
      sprintf(query, QUERY_POP, code);
      cur_mem->execute(query);
      column_names *cols = cur_mem->get_columns();
      Serial.println();
      row_values *row = NULL;
      int number = 0; 
    
      while (row = cur_mem->get_next_row()) {
        
        if (row != NULL) {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print(F("BON DIA"));
          lcd.setCursor(0,1);
          lcd.print(row->values[1]);
          idusuari = atol(row->values[0]); 
          Serial.print(idusuari);
          Serial.print(row->values[1]); //Nombre alumno
          delay(2000);

          if (client2.connect(server, 80)) {
            Serial.println(F("connected"));
            int value2 = 2; // Numero de arduino
            client2.print(F("GET /arduino.php?valor="));
            client2.print(idusuari);
            client2.print(F("&valor2="));
            client2.print(value2);
            client2.println(F(" HTTP/1.0"));
            Serial.println(F("Insertar"));
            client2.println(F("Host: grup1.webamida.com"));
            client2.println(F("Connection: close"));
            client2.println(); 
            lcd.clear();
            lcd.setCursor(0,0);  
            lcd.print(F("Registrar"));
            lcd.setCursor(0,1);
            client2.stop();
          } else {
            Serial.println(F("Fallo"));
            Serial.println(client2.status());
          }
   
          Serial.println(F("Registrant Alumne"));
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print(F("Registrant"));
          lcd.setCursor(0,1);
          lcd.print(F("Alumne"));
          Serial.println();
        }

      number++;
    }


    if(number==0){
      delete cur_mem;
      return false;
    }else{
      delete cur_mem;
      return true; 
    }
    
    delete cur_mem;
  }