import java.awt.EventQueue;
import java.awt.Font;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;

import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class vAfegir {

	JFrame frame;
	private JTextField txtNombre, txtTelefono;
	private JComboBox boxSalas, boxHoras;
	private JDateChooser dateChooser;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vAfegir window = new vAfegir();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vAfegir() {
		initialize();
	}

	private void initialize() {

		Date min = new Date();
		Date max = new Date();

		max.setMonth((max.getMonth() - 1 + 2) % 12 + 1);

		Calendar cal = Calendar.getInstance();
		cal.setTime(min);

		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				vMenu windowsMenu = new vMenu();
				windowsMenu.frame.setVisible(true);

			}
		});
		frame.setBounds(100, 100, 520, 370);
		frame.getContentPane().setLayout(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JLabel lblAfegir = new JLabel("Afegir");
		lblAfegir.setBounds(10, 11, 72, 24);
		lblAfegir.setFont(new Font("Arial", Font.BOLD, 20));
		frame.getContentPane().add(lblAfegir);

		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		txtNombre.setBounds(165, 91, 250, 25);
		txtNombre.setColumns(10);
		frame.getContentPane().add(txtNombre);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(165, 135, 250, 25);
		txtTelefono.setColumns(10);
		txtTelefono.setDocument(new JTextFieldLimit(9)); //Limitar a 9 caracteres.
		txtTelefono.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if(((caracter < '0') || (caracter > '9')) && (caracter != '\b')) {
					e.consume();
				}
			}
		});
		frame.getContentPane().add(txtTelefono);

		dateChooser = new JDateChooser();
		dateChooser.setBounds(165, 182, 250, 25);
		dateChooser.getJCalendar().setSelectableDateRange(min, max);
		frame.getContentPane().add(dateChooser);

		boxHoras = new JComboBox();
		boxHoras.setBounds(165, 233, 250, 25);
		boxHoras.addItem("");
		boxHoras.addItem("17:00");
		boxHoras.addItem("18:30");
		boxHoras.addItem("20:00");
		boxHoras.addItem("21:30");
		frame.getContentPane().add(boxHoras);

		JButton btnAfegir = new JButton("Afegir");
		btnAfegir.setFont(new Font("Arial", Font.PLAIN, 15));
		btnAfegir.setBounds(404, 295, 90, 25);
		frame.getContentPane().add(btnAfegir);

		JLabel lblNo = new JLabel("Nom");
		lblNo.setBounds(80, 92, 60, 24);
		lblNo.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblNo);

		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setBounds(80, 137, 70, 24);
		lblTelefon.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblTelefon);

		JLabel lblDia = new JLabel("Dia");
		lblDia.setBounds(80, 182, 46, 24);
		lblDia.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblDia);

		JLabel lblHora = new JLabel("Hora");
		lblHora.setBounds(80, 227, 60, 30);
		lblHora.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblHora);

		JLabel lblSala = new JLabel("Sala");
		lblSala.setBounds(80, 47, 60, 24);
		lblSala.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblSala);

		boxSalas = new JComboBox();
		boxSalas.setFont(new Font("Arial", Font.PLAIN, 15));
		boxSalas.setBounds(165, 47, 250, 25);
		boxSalas.addItem("");
		boxSalas.addItem("Sala 51");
		boxSalas.addItem("Sala Terror");
		boxSalas.addItem("Sala Carcel");
		boxSalas.addItem("Sala Detectives");
		frame.getContentPane().add(boxSalas);

		btnAfegir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clickInsert();
			}
		});
		
		JButton btnTornarAlMenu = new JButton("Tornar al Menu");
		btnTornarAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnTornarAlMenu.setBounds(10, 297, 135, 23);
		frame.getContentPane().add(btnTornarAlMenu);

	}

	public void clickInsert() {

		Date dateFromDateChooser = dateChooser.getDate();
		String dateString = String.format("%1$tY-%1$tm-%1$td", dateFromDateChooser);

		String stringHora = boxHoras.getSelectedItem().toString();
		String salaString = boxSalas.getSelectedItem().toString();
		String telefonString = txtTelefono.getText().toString();
		String nomString = txtNombre.getText().toString();
		

		
		if(salaString == null || salaString == ""){
			JOptionPane.showMessageDialog(null, "Camp SALA no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(dateString.equalsIgnoreCase("null-null-null")) {
			JOptionPane.showMessageDialog(null, "Camp DATA no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(stringHora == null || stringHora == "") {
			JOptionPane.showMessageDialog(null, "Camp HORA no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(telefonString.equalsIgnoreCase("") || telefonString.equalsIgnoreCase(null)){
			JOptionPane.showMessageDialog(null, "Camp TELEFON no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(nomString.equalsIgnoreCase("") || nomString.equalsIgnoreCase(null)){
			JOptionPane.showMessageDialog(null, "Camp NOM no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else {
			if (comprobarFechas(salaString, dateString, stringHora) == 0) {
				insertar();
			}
		}
	}

	public void insertar() {

		Date dateFromDateChooser = dateChooser.getDate();
		String dateString = String.format("%1$tY-%1$tm-%1$td", dateFromDateChooser);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception e1) {
			System.out.println(e1.toString());
		}

		try {

			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");

			String insertsql = "INSERT INTO reservas (idRoom ,nombre ,telefono ,date ,time) VALUES (?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(insertsql);

			ps.setString(1, boxSalas.getSelectedItem().toString());
			ps.setString(2, txtNombre.getText());
			ps.setString(3, txtTelefono.getText());
			ps.setString(4, dateString);
			ps.setString(5, boxHoras.getSelectedItem().toString());
			ps.addBatch();

			ps.executeBatch();
			con.close();

			JOptionPane.showMessageDialog(null, "S'ha guardat la reserva correctament.");

			frame.dispose();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}

	public int comprobarFechas(String sala, String dia, String hora) {

		int i = 0;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception e1) {
			System.out.println(e1.toString());
		}

		try {
			
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");

			String selectsql = "SELECT idRoom, date, time FROM reservas";
			PreparedStatement ps = con.prepareStatement(selectsql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				String salaBD = rs.getString(1);
				Date fecha = rs.getDate(2);
				String fechaBD = String.format("%1$tY-%1$tm-%1$td", fecha);
				String horaBD = rs.getString(3);

				if (dia.equalsIgnoreCase(fechaBD) && hora.equalsIgnoreCase(horaBD) && sala.equalsIgnoreCase(salaBD)) {
					i = 1;
				}

			}

			if(i == 1) {
				JOptionPane.showMessageDialog(null, "Aquest dia i hora ja estan OCUPATS per aquesta SALA.");
			}
			
			ps.executeBatch();
			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}

		return i;

	}

}
