import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class vLlistar {

	JFrame frame;
	private JTable table;
	private JComboBox comboBox, comboBoxSalas;
	private JLabel lblOrdenarPor;
	private String ordenar;
	private JTextField txtNombre;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vLlistar window = new vLlistar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vLlistar() {
		initialize();
		mostrarTaula();
	}

	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				vMenu windowsMenu = new vMenu();
				windowsMenu.frame.setVisible(true);
			}
		});
		frame.setBounds(100, 100, 645, 335);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblLlistar = new JLabel("Llistar");
		lblLlistar.setFont(new Font("Arial", Font.BOLD, 20));
		lblLlistar.setBounds(10, 11, 100, 14);
		frame.getContentPane().add(lblLlistar);

		table = new JTable();
		table.setFont(new Font("Arial", Font.PLAIN, 13));
		table.setEnabled(false);
		table.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		table.setBounds(10, 49, 609, 201);
		frame.getContentPane().add(table);
		
		lblOrdenarPor = new JLabel("Ordenar per:");
		lblOrdenarPor.setFont(new Font("Arial", Font.PLAIN, 13));
		lblOrdenarPor.setBounds(426, 14, 73, 14);
		frame.getContentPane().add(lblOrdenarPor);
		
		
		comboBox = new JComboBox();
		comboBox.setBounds(509, 11, 110, 20);
		comboBox.addItem("");
		comboBox.addItem("Salas");
		comboBox.addItem("Sala");
		comboBox.addItem("Data");
		comboBox.addItem("Nombre");
		frame.getContentPane().add(comboBox);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				elegirOrdenar();
				
			}
		});
		
		comboBoxSalas = new JComboBox();
		comboBoxSalas.setBounds(509, 11, 110, 20);
		comboBoxSalas.addItem("Sala 51");
		comboBoxSalas.addItem("Sala Terror");
		comboBoxSalas.addItem("Sala Carcel");
		comboBoxSalas.addItem("Sala Detectives");
		comboBoxSalas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				elegirOrdenar();
				
			}
		});

		JButton btnTornarAlMenu = new JButton("Tornar al Menu");
		btnTornarAlMenu.setBounds(10, 262, 135, 23);
		btnTornarAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		
		frame.getContentPane().add(btnTornarAlMenu);
		
		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		txtNombre.setBounds(509, 11, 110, 20);
		txtNombre.setColumns(10);
		
		txtNombre.addKeyListener(new KeyAdapter() {
	         public void keyPressed(KeyEvent e) {
	            if (e.getKeyCode()==KeyEvent.VK_ENTER) {
	            	actualizarDatos3(txtNombre.getText().toString());
	            }
	         }
	      });
		
		
	}

	public void mostrarTaula() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");
			
			String selectsql = "SELECT idRoom, nombre, telefono, date, time FROM reservas";
			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();

			table.setModel(DbUtils.resultSetToTableModel(rs));

			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}
	
	public void elegirOrdenar() {
		
		if (comboBox.getSelectedItem().toString().equalsIgnoreCase("Salas")){
			ordenar = "idRoom";
			
			comboBoxSalas.setVisible(false);
			txtNombre.setVisible(false);
			
			lblOrdenarPor.setBounds(426, 14, 73, 14);
			comboBox.setBounds(509, 11, 110, 20);
			
			actualizarDatos(ordenar);
		}else if (comboBox.getSelectedItem().toString().equalsIgnoreCase("Data")){
			ordenar = "date";
			
			comboBoxSalas.setVisible(false);
			txtNombre.setVisible(false);
			
			lblOrdenarPor.setBounds(426, 14, 73, 14);
			comboBox.setBounds(509, 11, 110, 20);
			
			actualizarDatos(ordenar);
		}else if(comboBox.getSelectedItem().toString().equalsIgnoreCase("Sala")) {
			frame.getContentPane().add(comboBoxSalas);
			
			comboBox.setBounds(390, 11, 110, 20);
			lblOrdenarPor.setBounds(307, 14, 73, 14);
			
			txtNombre.setVisible(false);
			comboBoxSalas.setVisible(true);
			
			actualizarDatos2();
		}else if(comboBox.getSelectedItem().toString().equalsIgnoreCase("Nombre")) {
			frame.getContentPane().add(txtNombre);
			
			comboBox.setBounds(390, 11, 110, 20);
			lblOrdenarPor.setBounds(307, 14, 73, 14);
			
			comboBoxSalas.setVisible(false);
			txtNombre.setVisible(true);
		}
		
	}
	
	public void actualizarDatos(String ordenar) {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC","boris", "boris");
			
			String selectsql = "SELECT idRoom, nombre, telefono, date, time FROM reservas ORDER BY " + ordenar+" ASC ";
			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();

			table.setModel(DbUtils.resultSetToTableModel(rs));

			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}
	
	public void actualizarDatos2() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC","boris", "boris");
			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
			
			String selectsql = "SELECT idRoom, nombre, telefono, date, time FROM reservas WHERE idRoom = '"+comboBoxSalas.getSelectedItem().toString()+"'";
					
			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();

			table.setModel(DbUtils.resultSetToTableModel(rs));

			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}
	
	public void actualizarDatos3(String nombre) {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC","boris", "boris");
			
			String selectsql = "SELECT idRoom, nombre, telefono, date, time FROM reservas WHERE nombre  = '"+nombre+"'";
			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();
			
			table.setModel(DbUtils.resultSetToTableModel(rs));
			
			if(rs.first() != true) {
				JOptionPane.showMessageDialog(null, "No s'ha trobat cap reserva amb aquest nom.");
			}

			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}
	
}
