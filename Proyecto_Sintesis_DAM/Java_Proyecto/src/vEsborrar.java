import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import net.proteanit.sql.DbUtils;
import javax.swing.border.*;

public class vEsborrar {

	JFrame frame;
	private JTable table;
	private JButton btnEsborrar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vEsborrar window = new vEsborrar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vEsborrar() {
		initialize();
		mostrarTaula();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 645, 335);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosed(WindowEvent arg0) {
				vMenu vMenu = new vMenu();
				vMenu.frame.setVisible(true);
			}
		});
		
		JLabel lblMostrar = new JLabel("Esborrar");
		lblMostrar.setFont(new Font("Arial", Font.BOLD, 15));
		lblMostrar.setBounds(10, 11, 90, 14);
		frame.getContentPane().add(lblMostrar);

		table = new JTable();
		table.setFont(new Font("Arial", Font.PLAIN, 13));
		table.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		table.setBounds(10, 49, 609, 201);
		frame.getContentPane().add(table);
		
		btnEsborrar = new JButton("Esborrar");
		btnEsborrar.setBounds(530, 261, 89, 23);
		frame.getContentPane().add(btnEsborrar);
		
		btnEsborrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "Estas segur que vols eliminar la reserva?", "WARNING", 
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					eliminar();
				}
				
			}
		});
		
		JButton btnTornarAlMenu = new JButton("Tornar al Menu");
		btnTornarAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnTornarAlMenu.setBounds(10, 261, 135, 23);
		frame.getContentPane().add(btnTornarAlMenu);
		

	}

	public void mostrarTaula() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");
			
			String selectsql = "SELECT * FROM reservas";
			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();

			table.setModel(DbUtils.resultSetToTableModel(rs));

			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}

	public void eliminar() {
		int column = 0;
		int row = table.getSelectedRow();
		String value = table.getModel().getValueAt(row, column).toString();
		
		int column2 = 2; 
		int row2 = table.getSelectedRow();
		String value2 = table.getModel().getValueAt(row2, column2).toString();
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");
			
			String deletesql = "DELETE FROM reservas WHERE id="+value+"";
			PreparedStatement ps = con.prepareStatement(deletesql);
			ps.executeUpdate();

			JOptionPane.showMessageDialog(null, "La reserva de "+value2+" ha sigut eliminada.");
			
			mostrarTaula();
			
			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}
}
