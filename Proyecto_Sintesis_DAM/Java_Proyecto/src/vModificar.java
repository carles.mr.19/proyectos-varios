import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import javax.swing.JComboBox;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

public class vModificar {

	JFrame frame;
	private String id;
	private JTextField txtNombre, txtTelefono;
	private JComboBox boxSalas, boxHoras;
	private JDateChooser dateChooser;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vModificar window = new vModificar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vModificar() {
		initialize();
		rellenarCampos();
	}

	private void initialize() {

		Date min = new Date();
		Date max = new Date();

		max.setMonth((max.getMonth() - 1 + 2) % 12 + 1);

		Calendar cal = Calendar.getInstance();
		cal.setTime(min);

		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				vMenu windowsMenu = new vMenu();
				windowsMenu.frame.setVisible(true);
				
				modificarSalir();

			}
		});
		frame.setBounds(100, 100, 520, 370);
		frame.getContentPane().setLayout(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JLabel lblAfegir = new JLabel("Modificar");
		lblAfegir.setBounds(10, 11, 90, 24);
		lblAfegir.setFont(new Font("Arial", Font.BOLD, 20));
		frame.getContentPane().add(lblAfegir);

		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		txtNombre.setBounds(165, 91, 250, 25);
		txtNombre.setColumns(10);
		frame.getContentPane().add(txtNombre);

		txtTelefono = new JTextField();
		txtTelefono.setFont(new Font("Arial", Font.PLAIN, 15));
		txtTelefono.setBounds(165, 135, 250, 25);
		txtTelefono.setColumns(10);
		txtTelefono.setDocument(new JTextFieldLimit(9)); //Limitar a 9 caracteres.
		txtTelefono.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if(((caracter < '0') || (caracter > '9')) && (caracter != '\b')) {
					e.consume();
				}
			}
		});
		frame.getContentPane().add(txtTelefono);

		dateChooser = new JDateChooser();
		dateChooser.setBounds(165, 182, 250, 25);
		dateChooser.getJCalendar().setSelectableDateRange(min, max);
		frame.getContentPane().add(dateChooser);

		boxHoras = new JComboBox();
		boxHoras.setFont(new Font("Arial", Font.PLAIN, 15));
		boxHoras.setBounds(165, 233, 250, 25);
		boxHoras.addItem("");
		boxHoras.addItem("17:00");
		boxHoras.addItem("18:30");
		boxHoras.addItem("20:00");
		boxHoras.addItem("21:30");
		frame.getContentPane().add(boxHoras);

		JButton btnModificar = new JButton("Modificar");
		btnModificar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnModificar.setBounds(394, 295, 100, 25);
		frame.getContentPane().add(btnModificar);

		JLabel lblNo = new JLabel("Nom");
		lblNo.setBounds(80, 92, 60, 24);
		lblNo.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblNo);

		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setBounds(80, 137, 70, 24);
		lblTelefon.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblTelefon);

		JLabel lblDia = new JLabel("Dia");
		lblDia.setBounds(80, 182, 46, 24);
		lblDia.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblDia);

		JLabel lblHora = new JLabel("Hora");
		lblHora.setBounds(80, 227, 60, 30);
		lblHora.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblHora);

		JLabel lblSala = new JLabel("Sala");
		lblSala.setBounds(80, 47, 60, 24);
		lblSala.setFont(new Font("Arial", Font.PLAIN, 20));
		frame.getContentPane().add(lblSala);

		boxSalas = new JComboBox();
		boxSalas.setFont(new Font("Arial", Font.PLAIN, 15));
		boxSalas.setBounds(165, 47, 250, 25);
		boxSalas.addItem("");
		boxSalas.addItem("Sala 51");
		boxSalas.addItem("Sala Terror");
		boxSalas.addItem("Sala Carcel");
		boxSalas.addItem("Sala Detectives");
		frame.getContentPane().add(boxSalas);
		
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "Estas segur que vols modificar la reserva?", "WARNING", 
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					clickModificar();
				}
				
			}
		});
		
		JButton btnTornarAlMenu = new JButton("Tornar al Menu");
		btnTornarAlMenu.setFont(new Font("Arial", Font.PLAIN, 15));
		btnTornarAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnTornarAlMenu.setBounds(10, 297, 135, 23);
		frame.getContentPane().add(btnTornarAlMenu);

	}
	
	public void clickModificar() {

		Date dateFromDateChooser = dateChooser.getDate();
		String dateString = String.format("%1$tY-%1$tm-%1$td", dateFromDateChooser);

		String horaString = boxHoras.getSelectedItem().toString();
		String salaString = boxSalas.getSelectedItem().toString();
		String telefonString = txtTelefono.getText().toString();
		String nomString = txtNombre.getText().toString();
		
		if(salaString == null || salaString == ""){
			JOptionPane.showMessageDialog(null, "Camp SALA no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(dateString.equalsIgnoreCase("null-null-null")) {
			JOptionPane.showMessageDialog(null, "Camp DATA no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(horaString == null || horaString == "") {
			JOptionPane.showMessageDialog(null, "Camp HORA no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(telefonString.equalsIgnoreCase("") || telefonString.equalsIgnoreCase(null)){
			JOptionPane.showMessageDialog(null, "Camp TELEFON no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else if(nomString.equalsIgnoreCase("") || nomString.equalsIgnoreCase(null)){
			JOptionPane.showMessageDialog(null, "Camp NOM no pot estar buit.", "ERROR", JOptionPane.WARNING_MESSAGE);
		}else {
			if (comprobarFechas(salaString, dateString, horaString) == 0) {
				  modificar();
			}
		}
	}

	public void rellenarCampos() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}

		try {

			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");
			
			String selectsql = "SELECT *  FROM reservas WHERE editable = 1";

			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
			
				id = rs.getString(1);
								
				if(rs.getString(2).equalsIgnoreCase("Sala 51")) {
					boxSalas.setSelectedIndex(1);
				}else if(rs.getString(2).equalsIgnoreCase("Sala Terror")) {
					boxSalas.setSelectedIndex(2);
				}else if(rs.getString(2).equalsIgnoreCase("Sala Carcel")) {
					boxSalas.setSelectedIndex(3);
				}else if(rs.getString(2).equalsIgnoreCase("Sala Detectives")) {
					boxSalas.setSelectedIndex(4);
				}
				
				txtNombre.setText(rs.getString(3));
				txtTelefono.setText(rs.getString(4));
				boxHoras.setSelectedItem(rs.getString(5));
				dateChooser.setDate(rs.getDate(6));
						
			}
			
			con.close();

		} catch (SQLException ex) {
			ex.getMessage();
		}
	}

	public void modificar() {
		Date dateFromDateChooser = dateChooser.getDate();
		String dateString = String.format("%1$tY-%1$tm-%1$td", dateFromDateChooser);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");

			String selectsql = "UPDATE reservas " + "SET idRoom = '"+boxSalas.getSelectedItem().toString()+"', nombre = '" + txtNombre.getText() + "'" + ",telefono = '"
					+ txtTelefono.getText() + "'" + ",date = '" + dateString + "'" + ",time = '"
					+ boxHoras.getSelectedItem().toString() + "'" + ",editable = 0 " + "WHERE id ='" + id + "'";

			PreparedStatement ps = con.prepareStatement(selectsql);
			ps.executeUpdate();

			JOptionPane.showMessageDialog(null, "S'ha modificat la reserva correctament.");
			
			modificarSalir();
			
			frame.dispose();
			
			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}
	
	public void modificarSalir() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");

			String updatesql = "UPDATE reservas SET editable = 0 WHERE id ='" + id + "'";

			PreparedStatement ps = con.prepareStatement(updatesql);
			ps.executeUpdate();
			
			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}
	
	public int comprobarFechas(String sala, String dia, String hora) {
		//rs.getString(1); ID  * rs.getString(2); SALA  * rs.getString(3); Nombre Cliente
		//rs.getString(4); Telefon Cliente *  rs.getString(5); Dia * rs.getString(6); Hora		
		int i = 0;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception e1) {
			System.out.println(e1.toString());
		}

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");

			String selectsql = "SELECT * FROM reservas";
			PreparedStatement ps = con.prepareStatement(selectsql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String salaBD = rs.getString(2);
				Date fecha = rs.getDate(6);
				String horaBD = rs.getString(5);
				String fechaBD = String.format("%1$tY-%1$tm-%1$td", fecha);
				
				if (dia.equalsIgnoreCase(fechaBD) && hora.equalsIgnoreCase(horaBD) && sala.equalsIgnoreCase(salaBD)) {
					i = 1;
				}

			}

			if(i == 1) {
				JOptionPane.showMessageDialog(null, "Aquest dia i hora ja estan OCUPATS per aquesta SALA.");
			}
			
			ps.executeBatch();
			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}

		return i;

	}
	
	
	
}
