import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import net.proteanit.sql.DbUtils;
import javax.swing.border.*;

public class vSeleccionarModificar {

	JFrame frame;
	private JTable table;
	private JButton btnEsborrar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vSeleccionarModificar window = new vSeleccionarModificar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vSeleccionarModificar() {
		initialize();
		mostrarTaula();
	}

	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosed(WindowEvent arg0) {
				vMenu vMenu = new vMenu();
				vMenu.frame.setVisible(true);
			}
		});
		frame.setBounds(100, 100, 645, 335);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblMostrar = new JLabel("Seleccionar Modificar");
		lblMostrar.setFont(new Font("Arial", Font.BOLD, 15));
		lblMostrar.setBounds(10, 11, 185, 14);
		frame.getContentPane().add(lblMostrar);

		table = new JTable();
		table.setFont(new Font("Arial", Font.PLAIN, 13));
		table.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		table.setBounds(10, 49, 609, 201);
		frame.getContentPane().add(table);

		btnEsborrar = new JButton("Modificar");
		btnEsborrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				idEliminar();
				frame.setVisible(false);
				vModificar windowModificar = new vModificar();
				windowModificar.frame.setVisible(true);
			}
		});
		btnEsborrar.setBounds(530, 261, 89, 23);
		frame.getContentPane().add(btnEsborrar);
		
		JButton btnTornarAlMenu = new JButton("Tornar al Menu");
		btnTornarAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnTornarAlMenu.setBounds(10, 262, 135, 23);
		frame.getContentPane().add(btnTornarAlMenu);		

	}

	public void mostrarTaula() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");
			
			String selectsql = "SELECT id, idRoom, nombre, telefono, date, time FROM reservas";
			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();

			table.setModel(DbUtils.resultSetToTableModel(rs));

			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}
	}

	public void idEliminar() {
		int row = table.getSelectedRow();
		String value = table.getModel().getValueAt(row, 0).toString();
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (Exception a) {
			System.out.println(a.toString());
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");

			String updatesql = "UPDATE reservas SET editable = 1 WHERE id ='"+value+"'";

			PreparedStatement ps = con.prepareStatement(updatesql);
			ps.executeUpdate();

			con.close();

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
		}

		

	}
}
