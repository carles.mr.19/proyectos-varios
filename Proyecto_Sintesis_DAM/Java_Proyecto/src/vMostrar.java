import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;

import net.proteanit.sql.DbUtils;

import java.awt.Font;

import javax.swing.JLabel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class vMostrar {

	JFrame frame;
	private JTable table;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vMostrar window = new vMostrar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vMostrar() {
		initialize();
		mostrarTaula();
	}

	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				vMenu windowsMenu = new vMenu();
				windowsMenu.frame.setVisible(true);
			}
		});

		frame.setBounds(100, 100, 645, 335);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		table = new JTable();
		table.setFont(new Font("Arial", Font.PLAIN, 13));
		table.setEnabled(false);
		table.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		table.setBounds(10, 49, 609, 201);
		frame.getContentPane().add(table);

		JLabel lblMostrar = new JLabel("Mostrar");
		lblMostrar.setFont(new Font("Arial", Font.BOLD, 20));
		lblMostrar.setBounds(10, 11, 100, 14);
		frame.getContentPane().add(lblMostrar);
		
		JButton btnTornarAlMenu = new JButton("Tornar al Menu");
		btnTornarAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnTornarAlMenu.setBounds(10, 262, 135, 23);
		frame.getContentPane().add(btnTornarAlMenu);

	}

	public void mostrarTaula() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.31.24:3306/escaperoom?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "boris", "boris");
			
			String selectsql = "SELECT idRoom, nombre, telefono, date, time FROM reservas ORDER BY date";
			PreparedStatement ps = con.prepareStatement(selectsql);
			ResultSet rs = ps.executeQuery();

			table.setModel(DbUtils.resultSetToTableModel(rs));

			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
