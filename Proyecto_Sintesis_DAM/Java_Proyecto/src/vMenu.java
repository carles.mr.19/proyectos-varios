import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;

public class vMenu {

	JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vMenu window = new vMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vMenu() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Arial", Font.PLAIN, 13));
		frame.setBounds(100, 100, 340, 400);
		frame.getContentPane().setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel lblTitol = new JLabel("Gesti\u00F3 d'Escape Rooms");
		lblTitol.setBounds(10, 11, 304, 18);
		lblTitol.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitol.setFont(new Font("Arial", Font.BOLD, 20));
		frame.getContentPane().add(lblTitol);

		JButton bMostrar = new JButton("     Mostrar Reserva");
		bMostrar.setFont(new Font("Arial", Font.PLAIN, 13));
		bMostrar.setIcon(new ImageIcon("D:\\Proyectos Java\\Projecte_AplicacioPC\\img\\boton-de-menu(2).png"));
		bMostrar.setBounds(50, 51, 224, 50);
		frame.getContentPane().add(bMostrar);

		bMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				vMostrar mostrarWindow = new vMostrar();
				mostrarWindow.frame.setVisible(true);
			}
		});

		JButton bModificar = new JButton(" Modificar Reserves");
		bModificar.setFont(new Font("Arial", Font.PLAIN, 13));
		bModificar.setIcon(new ImageIcon("D:\\Proyectos Java\\Projecte_AplicacioPC\\img\\modificar.png"));
		bModificar.setBounds(50, 234, 224, 50);
		frame.getContentPane().add(bModificar);

		bModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				vSeleccionarModificar modificarWindow = new vSeleccionarModificar();
				modificarWindow.frame.setVisible(true);
			}
		});

		JButton bCrear = new JButton("     Afegir Reserva");
		bCrear.setFont(new Font("Arial", Font.PLAIN, 13));
		bCrear.setIcon(new ImageIcon("D:\\Proyectos Java\\Projecte_AplicacioPC\\img\\a\u00F1adir.png"));
		bCrear.setBounds(50, 173, 224, 50);
		frame.getContentPane().add(bCrear);

		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				vAfegir crearWindow = new vAfegir();
				crearWindow.frame.setVisible(true);
			}
		});

		JButton bEliminar = new JButton("     Esborrar Reserva");
		bEliminar.setFont(new Font("Arial", Font.PLAIN, 13));
		bEliminar.setIcon(new ImageIcon("D:\\Proyectos Java\\Projecte_AplicacioPC\\img\\eliminar.png"));
		bEliminar.setBounds(50, 295, 224, 50);
		frame.getContentPane().add(bEliminar);

		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				vEsborrar esborrarWindow = new vEsborrar();
				esborrarWindow.frame.setVisible(true);
			}
		});

		JButton bLlistar = new JButton("     Llistar Reserves");
		bLlistar.setFont(new Font("Arial", Font.PLAIN, 13));
		bLlistar.setIcon(new ImageIcon("D:\\Proyectos Java\\Projecte_AplicacioPC\\img\\listado.png"));
		bLlistar.setBounds(50, 112, 224, 50);
		frame.getContentPane().add(bLlistar);

		bLlistar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				vLlistar llistarWindow = new vLlistar();
				llistarWindow.frame.setVisible(true);
			}
		});

	}
}
