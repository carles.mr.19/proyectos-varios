import javax.swing.*;
import java.awt.Font;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class vCarga extends JFrame {

	private JProgressBar current;
	private int num = 0;
	private static vCarga frame;
	private JTable table;
	private DefaultTableModel model = new DefaultTableModel();

	public static void main(String[] arguments) {
		frame = new vCarga();
		frame.pack();
		frame.setBounds(100, 100, 550, 350);
		frame.setVisible(true);
		frame.iterate();
	}

	public vCarga() {
		initialize();
	}

	private void initialize() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel pane = new JPanel();
		pane.setBounds(100, 100, 550, 350);
		pane.setLayout(null);

		JLabel lblGestioEscapeRoom = new JLabel("Gesti\u00F3 d'Escape Rooms");
		lblGestioEscapeRoom.setFont(new Font("Arial", Font.BOLD, 20));
		lblGestioEscapeRoom.setHorizontalAlignment(SwingConstants.CENTER);
		lblGestioEscapeRoom.setBounds(10, 11, 509, 24);
		pane.add(lblGestioEscapeRoom);

		table = new JTable(model);
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);

		table.setEnabled(false);
		model.addColumn(null);

		table.setFont(new Font("Arial", Font.PLAIN, 20));
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setBounds(10, 46, 509, 213);
		pane.add(table);

		current = new JProgressBar(0, 2000);
		current.setBounds(10, 270, 509, 30);
		current.setValue(0);
		current.setStringPainted(true);

		pane.add(current);
		setContentPane(pane);

	}

	public void iterate() {

		while (num < 2000) {
			current.setValue(num);

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}

			if (num == 285) {
				model.insertRow(0, new Object[] { current.getString() + "  -  " + "Iniciant base de dades" });
				table.setRowHeight(0, 30);
			} else if (num == 950) {
				model.insertRow(1, new Object[] { current.getString() + "  -  " + "Traspassant dades a l'aplicació" });
				table.setRowHeight(1, 30);
			} else if (num == 1520) {
				model.insertRow(2, new Object[] { current.getString() + "  -  " + "Iniciant aplicació" });
				table.setRowHeight(2, 30);
			} else if (num == 1995) {
				frame.dispose();
				vMenu menuWindow = new vMenu();
				menuWindow.frame.setVisible(true);
			}

			num += 95;
		}
	}
}
