import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ventana2 extends JFrame {
	
	private JTextField pantalla1;
	private JTextField pantalla2;
	
	public Ventana2(){
		super("Ventana2");
		setSize(400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		//Label1
		JPanel panel1 = new JPanel();
		GridBagConstraints gbc = new GridBagConstraints();
		panel1.setLayout(new GridBagLayout());
		gbc.insets = new Insets(0,0,20,20);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel1.add(new JLabel("Institut Jaume Huguet"),gbc);
	
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel1.add(new JLabel("PREMIOS: "),gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		panel1.add(new JLabel("GRUPO"),gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		panel1.add(new JLabel("ALUMNO"),gbc);
		
		this.pantalla1 = new JTextField(10);
		gbc.gridx = 0;
		gbc.gridy = 3;		
		panel1.add(pantalla1,gbc);
		
		
		this.pantalla2 = new JTextField(10);
		gbc.gridx = 1;
		gbc.gridy = 3;		
		panel1.add(pantalla2,gbc);
		
		JButton teclaAnterior = new JButton("Anterior");
		gbc.gridx = 0;
		gbc.gridy = 4;
		panel1.add(teclaAnterior,gbc);
		
		JButton teclaSiguiente = new JButton("Siguiente");
		gbc.gridx = 1;
		gbc.gridy = 4;
		panel1.add(teclaSiguiente,gbc);
		
		//CONTANINER
		Container cp = getContentPane();
		cp.add(panel1);

		
	}
	
}
