import java.io.*;

public class Utilidad {
	
	public void LeerFichero() {

		      File archivo = null;
		      FileReader fr = null;
		      BufferedReader br = null;

		      try {
		         archivo = new File ("C:\\Users\\carle\\Desktop\\alumnos.txt");
		         fr = new FileReader (archivo);
		         br = new BufferedReader(fr);

		         String linea;
		         while((linea=br.readLine())!=null)
		            System.out.println(linea);
		      }
		      catch(Exception e){
		         e.printStackTrace();
		      }finally{
		         try{                    
		            if( null != fr ){   
		               fr.close();     
		            }                  
		         }catch (Exception e2){ 
		            e2.printStackTrace();
		         }
		      }
		   }
	
	public void EscribirFichero() {

	        FileWriter fichero = null;
	        PrintWriter pw = null;
	        try
	        {
	            fichero = new FileWriter("C:\\Users\\carle\\Desktop\\resultado.txt");
	            pw = new PrintWriter(fichero);

	            for (int i = 0; i < 10; i++)
	                pw.println("Linea " + i);

	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	           try {
	           if (null != fichero)
	              fichero.close();
	           } catch (Exception e2) {
	              e2.printStackTrace();
	           }
	        }
	    }
	
	
	}

