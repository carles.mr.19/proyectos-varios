import java.io.*;
import java.util.*;
import javax.swing.*;

public class HacerSorteo {
	
	private ArrayList<String> alumnos;
	private ArrayList<String> premios;
	
	private int nAlumnos;
	private int nPremios;
	
	Ventana1 ventana1;
	
	public HacerSorteo(){
		this.alumnos = new ArrayList<String>();
		this.premios = new ArrayList<String>();
	}
	
	public void leerAlumnos() {
				   
		File archivo = null;
	    FileReader fr = null;
	    BufferedReader br = null;

	    try {
	    	archivo = new File ("ARCHIVO DE ALUMNOS"); //CAMBIAR DIRECCION FICHERO
	    	fr = new FileReader (archivo);
	        br = new BufferedReader(fr);

	        String linea;
	        while((linea=br.readLine())!=null)
	            alumnos.add(linea);
	      }
	      catch(Exception e){
	        e.printStackTrace();
	      }finally{

	        try{                    
	           if( null != fr ){   
	              fr.close();     
	           }                  
	        }catch (Exception e2){ 
	           e2.printStackTrace();
	        }
	      }
	}
	
	public void leerPremios() {

	      File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;

	      try {
	         archivo = new File ("ARCHIVOS DE PREMIOS"); //CAMBIAR DIRECCION FICHERO
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);

	         String linea;
	         while((linea=br.readLine())!=null)
	        	 premios.add(linea);	
	      }
	  	      
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
	   }

	
	public void Random() {
		int i;
		
		nAlumnos = alumnos.size();
		nPremios = premios.size();
		
		int nAlumnosRandom[] = new int[nAlumnos];
		
		for(i = 0; i < nPremios; i++){
			//GRUPO Y NOMBRE ALUMNOS
			nAlumnosRandom[i] =  (int) (Math.random()*nAlumnos);
			System.out.println("Alumno : "+alumnos.get(nAlumnosRandom[i]));
			
			//NOMBRE PREMIO
			premios.get(i);
			System.out.println("Premio: "+premios.get(i));
			
		}
			
			/*PARA QUE NO SE REPITA
				for(j = 0 ; j < 1; j++){
					if(nAlumnosRandom[i] == nAlumnosRandom[j]){
						i--;
					}
				}
			*/

		}
		
}
