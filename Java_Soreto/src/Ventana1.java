import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Ventana1 extends JFrame {
	
	private JTextField pantallaAlumnos;
	private JTextField pantallaPremios;
	
	public Ventana1(){		
		super("Ventana1");
		setSize(400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		//Label1
		JPanel panel1 = new JPanel();
		GridBagConstraints gbc = new GridBagConstraints();
		panel1.setLayout(new GridBagLayout());
		gbc.insets = new Insets(0,0,20,20);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel1.add(new JLabel("Institut Jaume Huguet"),gbc);
	
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel1.add(new JLabel("Fichero alumnos: "),gbc);
		
		this.pantallaAlumnos = new JTextField(20);
		gbc.gridx = 0;
		gbc.gridy = 2;		
		panel1.add(pantallaAlumnos,gbc);
		
		JButton teclaBuscar = new JButton("Buscar");
		gbc.gridx = 1;
		gbc.gridy = 2;
		panel1.add(teclaBuscar,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		panel1.add(new JLabel("Fichero Premios: "),gbc);

		this.pantallaPremios = new JTextField(20);
		gbc.gridx = 0;
		gbc.gridy = 4;		
		panel1.add(pantallaPremios,gbc);
		
		JButton teclaEjecutar = new JButton("Ejecutar");
		gbc.gridx = 0;
		gbc.gridy = 5;
		teclaEjecutar.addActionListener(new EjecutarSorteo());
		panel1.add(teclaEjecutar,gbc);

		//CONTANINER
		Container cp = getContentPane();
		cp.add(panel1);

		
	}
	
	class EjecutarSorteo implements ActionListener {
		public void actionPerformed(ActionEvent a) {
			HacerSorteo hs = new HacerSorteo();
			hs.leerAlumnos();
			hs.leerPremios();
			hs.Random();
			//System.out.println("EJECUTADO");
		}
	}

	public JTextField getPantallaAlumnos() {
		return pantallaAlumnos;
	}

	public JTextField getPantallaPremios() {
		return pantallaPremios;
	}
	
	
	
}
