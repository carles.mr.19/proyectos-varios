# Proyectos Varios

Aquí podras ver todos mis proyectos que he creado con anterioridad.
Here you can see all my projects that I created previously.

Java_Sorteo

It is a program that allows us to make a raffle, we need some fitxeros with a list of people with a specific format and a list of prizes.
With a simple graphical interface and very easy to use.

Proyecto Sintesis DAM

It is a part of the final project of the higher degree of cross-platform application development.
This consisted of making a Java application using JSwing to manage the different reservations that users make through a mobile application.

Proyecto_Sintesis_SMX

It is part of the final project of the middle grade which consisted of creating a web page, showing the product that we had created.
Our product was a small Arduino board which registered the entry time of a student through a code.